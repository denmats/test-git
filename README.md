CREATE CLOUDSQL PROXY DEPLOYMENT TO CONNECT WITH CLOUD SQL

before starting:
	create service-account:
		add roles cloud Admin, 
		create credentials.json and save it in working directory 

	create cloud sql
	create kubernetes cluster
	* cloud sql and cluster must be in the same compute zone
	
	create user to manage the cloud sql:
		username = proxyuser, userpassword = 123
	
	create two secrets:
		cloudsql-instance-credentials
		cloudsql-db-credentials

	create image 'toolbox' containing mysql client to connect with cloud sql through:
	docker build -t gcr.io./[PROJECT_ID]/toolbox:1.0 .

	push image 'gcr.io./[PROJECT_ID]/toolbox:1.0'

starting:
	*deployment.yaml :
			- change in image toolbox [PROJECT_ID],
			-instances=[YOUR_CLOUDSQL_CONNECTION]=tcp:3306
	*toolbox.yaml:
			- change in image toolbox [PROJECT_ID]

	run next commands:
		kubectl create -f deployment.yaml
		kubectl create -f toolbox.yaml

	in few minutes run:
		kubectl get pods
	from the output take name of the pod and paste it in db-mysql-fill-data.sh

	run:
		sh db-mysql-fill-data.sh

