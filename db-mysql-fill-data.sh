#!/bin/bash

#Change [YOUR MYSQL POD]. name of your mysql pod you can take from: kubectl get pods --namespace=dev1
kubectl exec -it demo-688d8c5899-vw948  -- mysql -uproxyuser -p123 -h 127.0.0.1<<EOF
/*create database mydb*/
create database if not exists mydb;

/*turn into mydb*/
use mydb;

/*create tables: users, bookings, showtimes, movies*/
create table if not exists users(id_users varchar(255) primary key, name varchar(255), last_active varchar(255));

/*fill table notes some data*/
insert into users values('chris_rivers','Chris Rivers','1360031010'),('peter_curley','Peter Curley','1360031222'),('garret_heaton','Garret Heaton','1360031425');
EOF

echo -e  "The database 'mydb' is created, table 'users' is created, some test records are in the table 'users'"

